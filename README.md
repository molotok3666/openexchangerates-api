# Как поднят проект в 1й раз:
* в директории ./app скопировать .env.example в .env
* заполнить следующие переменные в .env:
  DB_DATABASE=
  DB_USERNAME=
  DB_PASSWORD=
  DB_ROOT_PASSWORD=
  OXR_APP_ID=
* в ./app выполнить команду
```bash
docker-compose up -d
```
* выполнимть команду (генерация ключа приложения)
```bash
    docker exec -it php php artisan key:generate
```

# Для поднятия в последующие разы:
* выполнить в ./app
```bash
docker-compose up -d
```

# Внимание
Все DTO выводятся в стандратном dd() Laravel
На бесплатном тарифе не все паракметры доступны (вчастности, указание базовой валюты) => падают ошибки

# Роуты
* http://localhost/oxr/latest?symbols=RUB
* http://localhost/oxr/latest?date=2020-01-01&symbols=AED,AFN
* http://localhost/oxr/currencies?show_inactive=1
* http://localhost/oxr/time-series?start=2022-01-01&end=2022-01-15 - не доступно на бесплатном тарифе
* http://localhost/oxr/convert?value=123&from=USD&to=RUB - не доступно на бесплатном тарифе
* http://localhost/oxr/ohlc?start_time=2021-01-01T12:12:34Z&period=1h - не доступно на бесплатном тарифе
* http://localhost/oxr/usage