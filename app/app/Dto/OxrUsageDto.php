<?php
declare(strict_types=1);

namespace App\Dto;

use JsonException;

class OxrUsageDto
{
    private int $status;
    private array $data;

    /**
     * @throws JsonException
     */
    public function __construct(string $jsonData)
    {
        $data = json_decode($jsonData, true, 512, JSON_THROW_ON_ERROR);

        $this->status = $data['status'];
        $this->data = $data['data'];
    }

    public function getStatus(): mixed
    {
        return $this->status;
    }

    public function getData(): mixed
    {
        return $this->data;
    }
}
