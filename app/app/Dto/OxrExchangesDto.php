<?php
declare(strict_types=1);

namespace App\Dto;

use JsonException;

class OxrExchangesDto
{
    private string $disclaimer;
    private string $license;
    private int $timestamp;
    private string $base;
    private array $rates;

    /**
     * @throws JsonException
     */
    public function __construct(string $jsonData)
    {
        $data = json_decode($jsonData, true, 512, JSON_THROW_ON_ERROR);

        $this->disclaimer = $data['disclaimer'];
        $this->license = $data['license'];
        $this->timestamp = $data['timestamp'];
        $this->base = $data['base'];
        $this->rates = $data['rates'];
    }

    public function getDisclaimer(): string
    {
        return $this->disclaimer;
    }

    public function getLicense(): string
    {
        return $this->license;
    }

    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    public function getBase(): string
    {
        return $this->base;
    }

    public function getRates(): array
    {
        return $this->rates;
    }

    public function getConcreteRate(string $symbols): ?float
    {
        return $this->rates[$symbols] ?? null;
    }
}
