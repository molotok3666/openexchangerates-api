<?php
declare(strict_types=1);

namespace App\Dto;

use JsonException;

class OxrTimeSeriesDto
{
    private string $disclaimer;
    private string $license;
    private string $base;
    private array $rates;
    private string $startDate;
    private string $endDate;

    /**
     * @throws JsonException
     */
    public function __construct(string $jsonData)
    {
        $data = json_decode($jsonData, true, 512, JSON_THROW_ON_ERROR);

        $this->disclaimer = $data['disclaimer'];
        $this->license = $data['license'];
        $this->startDate = $data['start_date'];
        $this->endDate = $data['end_date'];
        $this->base = $data['base'];
        $this->rates = $data['rates'];
    }

    public function getDisclaimer(): string
    {
        return $this->disclaimer;
    }

    public function getLicense(): string
    {
        return $this->license;
    }

    public function getBase(): string
    {
        return $this->base;
    }

    public function getStartDate(): string
    {
        return $this->startDate;
    }

    public function getEndDate(): string
    {
        return $this->endDate;
    }

    public function getRates(): array
    {
        return $this->rates;
    }

    public function getConcreteRates(string $date): ?float
    {
        return $this->rates[$date] ?? null;
    }
}
