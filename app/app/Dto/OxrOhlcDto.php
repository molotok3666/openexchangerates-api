<?php
declare(strict_types=1);

namespace App\Dto;

use JsonException;

class OxrOhlcDto
{
    private string $disclaimer;
    private string $license;
    private string $startTime;
    private string $endTime;
    private string $base;
    private array $rates;

    /**
     * @throws JsonException
     */
    public function __construct(string $jsonData)
    {
        $data = json_decode($jsonData, true, 512, JSON_THROW_ON_ERROR);

        $this->disclaimer = $data['disclaimer'];
        $this->license = $data['license'];
        $this->startTime = $data['start_time'];
        $this->endTime = $data['end_time'];
        $this->base = $data['base'];
        $this->rates = $data['rates'];
    }

    public function getDisclaimer(): string
    {
        return $this->disclaimer;
    }

    public function getLicense(): string
    {
        return $this->license;
    }

    public function getStartTime(): string
    {
        return $this->startTime;
    }

    public function getEndTime(): string
    {
        return $this->endTime;
    }

    public function getBase(): string
    {
        return $this->base;
    }

    public function getRates(): array
    {
        return $this->rates;
    }
}
