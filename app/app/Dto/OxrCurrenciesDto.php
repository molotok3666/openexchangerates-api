<?php
declare(strict_types=1);

namespace App\Dto;

use JsonException;

class OxrCurrenciesDto
{
    private array $currencies;

    /**
     * @throws JsonException
     */
    public function __construct(string $jsonData)
    {
        $data = json_decode($jsonData, true, 512, JSON_THROW_ON_ERROR);

        $this->currencies = $data;
    }


    public function getConcrete(string $symbols): ?float
    {
        return $this->currencies[$symbols] ?? null;
    }
}
