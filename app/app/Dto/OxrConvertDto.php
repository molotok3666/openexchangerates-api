<?php
declare(strict_types=1);

namespace App\Dto;

use JsonException;

class OxrConvertDto
{
    private string $disclaimer;
    private string $license;
    private array $request;
    private array $meta;
    private float $response;

    /**
     * @throws JsonException
     */
    public function __construct(string $jsonData)
    {
        $data = json_decode($jsonData, true, 512, JSON_THROW_ON_ERROR);

        $this->disclaimer = $data['disclaimer'];
        $this->license = $data['license'];
        $this->request = $data['request'];
        $this->meta = $data['meta'];
        $this->response = $data['response'];
    }

    public function getDisclaimer(): string
    {
        return $this->disclaimer;
    }

    public function getLicense(): string
    {
        return $this->license;
    }

    public function getRequest(): mixed
    {
        return $this->request;
    }

    public function getMeta(): mixed
    {
        return $this->meta;
    }

    public function getResponse(): mixed
    {
        return $this->response;
    }

}
