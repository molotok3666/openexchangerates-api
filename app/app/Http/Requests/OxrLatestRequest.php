<?php
declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OxrLatestRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'base' => [
                'sometimes',
                'string',
                'size:3'
            ],
            'symbols' => [
                'sometimes',
                'string',
            ],
        ];
    }
}
