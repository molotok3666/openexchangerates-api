<?php
declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OxrOhlcRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'start_time' => [
                'required',
                'date_format:Y-m-d\TH:i:s\Z',
            ],
            'period' => [
                'required',
                'string',
            ],
            'base' => [
                'sometimes',
                'string',
                'size:3'
            ],
            'symbols' => [
                'sometimes',
                'string',
            ],
        ];
    }
}
