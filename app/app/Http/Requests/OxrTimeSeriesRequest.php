<?php
declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OxrTimeSeriesRequest extends FormRequest
{
    private const MAX_DATE = 'now';

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'start' => [
                'required',
                'date_format:Y-m-d',
            ],
            'end' => [
                'required',
                'date_format:Y-m-d',
                'before_or_equal:' . static::MAX_DATE,
            ],
            'base' => [
                'sometimes',
                'string',
                'size:3'
            ],
            'symbols' => [
                'sometimes',
                'string',
            ],
        ];
    }
}
