<?php
declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OxrHistoricalRequest extends FormRequest
{
    private const MIN_DATE = '1999-01-01',
        MAX_DATE = 'now';

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'date' => [
                'required',
                'date_format:Y-m-d',
                'after_or_equal:' . static::MIN_DATE,
                'before_or_equal:' . static::MAX_DATE,
            ],
            'base' => [
                'sometimes',
                'string',
                'size:3'
            ],
            'symbols' => [
                'sometimes',
                'string',
            ],
        ];
    }
}
