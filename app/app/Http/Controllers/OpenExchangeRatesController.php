<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\OxrConvertRequest;
use App\Http\Requests\OxrCurrenciesRequest;
use App\Http\Requests\OxrHistoricalRequest;
use App\Http\Requests\OxrLatestRequest;
use App\Http\Requests\OxrOhlcRequest;
use App\Http\Requests\OxrTimeSeriesRequest;
use App\Services\OpenExchangeRatesService;
use GuzzleHttp\Exception\GuzzleException;
use JetBrains\PhpStorm\NoReturn;
use JsonException;

class OpenExchangeRatesController extends Controller
{
    private OpenExchangeRatesService $openExchangeRatesService;

    public function __construct()
    {
        $this->openExchangeRatesService = new OpenExchangeRatesService;
    }

    /**
     * @throws GuzzleException
     * @throws JsonException
     */
    #[NoReturn] public function getLatest(OxrLatestRequest $request): void
    {
        $base = $request->get('base');
        $symbols = $request->get('symbols', '');

        $result = $this->openExchangeRatesService->getLatest($base, $symbols);

        dd($result);
    }

    /**
     * @throws GuzzleException
     * @throws JsonException
     */
    #[NoReturn] public function getHistorical(OxrHistoricalRequest $request): void
    {
        $date = $request->get('date');
        $base = $request->get('base');
        $symbols = $request->get('symbols', '');


        $result = $this->openExchangeRatesService->getHistorical($date, $base, $symbols);

        dd($result);
    }

    /**
     * @throws GuzzleException
     * @throws JsonException
     */
    #[NoReturn] public function getCurrencies(OxrCurrenciesRequest $request): void
    {
        $showInactive = $request->get('show_inactive');


        $result = $this->openExchangeRatesService->getCurrencies($showInactive);

        dd($result);
    }

    /**
     * @throws GuzzleException
     * @throws JsonException
     */
    #[NoReturn] public function getTimeSeries(OxrTimeSeriesRequest $request): void
    {
        $dateStart = $request->get('start');
        $dateEnd = $request->get('end');
        $base = $request->get('base');
        $symbols = $request->get('symbols', '');

        $result = $this->openExchangeRatesService->getTimeSeries($dateStart, $dateEnd, $base, $symbols);

        dd($result);
    }


    /**
     * @throws GuzzleException
     * @throws JsonException
     */
    #[NoReturn] public function convert(OxrConvertRequest $request): void
    {
        $value = (int)$request->get('value');
        $from = $request->get('from');
        $to = $request->get('to');

        $result = $this->openExchangeRatesService->convert($value, $from, $to);

        dd($result);
    }

    /**
     * @throws GuzzleException
     * @throws JsonException
     */
    #[NoReturn] public function getOhlc(OxrOhlcRequest $request): void
    {
        $startTime = $request->get('start_time');
        $period = $request->get('period');
        $symbols = $request->get('symbols');
        $base = $request->get('base');

        $result = $this->openExchangeRatesService->getOhcl($startTime, $period, $base, $symbols);

        dd($result);
    }

    /**
     * @throws GuzzleException
     * @throws JsonException
     */
    #[NoReturn] public function getUsage(): void
    {
        $result = $this->openExchangeRatesService->getUsage();

        dd($result);
    }
}
