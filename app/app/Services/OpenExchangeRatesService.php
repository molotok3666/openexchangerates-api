<?php
declare(strict_types=1);

namespace App\Services;

use App\Dto\OxrConvertDto;
use App\Dto\OxrCurrenciesDto;
use App\Dto\OxrExchangesDto;
use App\Dto\OxrOhlcDto;
use App\Dto\OxrTimeSeriesDto;
use App\Dto\OxrUsageDto;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;

class OpenExchangeRatesService
{
    private const BASE_DEFAULT = 'USD';

    private const URI_LATEST = 'latest.json',
        URI_CURRENCIES = 'currencies.json',
        URI_TIME_SERIES = 'time-series.json',
        URI_OHLC = 'ohlc.json',
        URI_USAGE = 'usage.json';

    private const URI_TEMPLATE_HISTORICAL = 'historical/%s.json',
        URI_TEMPLATE_CONVERT = 'convert/%s/%s/%s';

    private Client $client;

    private string $appId;

    private bool $prettyPrint = false;
    private bool $showAlternative = false;

    public function __construct()
    {
        $this->appId = config('open_exchange_rates.app_id');
        $this->client = new Client([
            'base_uri' => config('open_exchange_rates.api_url'),
            'query' => [
                'app_id' => $this->appId,
            ]
        ]);
    }

    public function setPrettyPrint(bool $prettyPrint = false): static
    {
        $this->prettyPrint = $prettyPrint;

        return $this;
    }

    public function setShowAlternative(bool $showAlternative = false): static
    {
        $this->showAlternative = $showAlternative;

        return $this;
    }

    /**
     * @throws GuzzleException|JsonException
     */
    public function getLatest(?string $base, ?string $symbols): OxrExchangesDto
    {
        $response = $this->client->get(static::URI_LATEST, [
            'query' => [
                'app_id' => $this->appId,
                'base' => $base ?? self::BASE_DEFAULT,
                'symbols' => $symbols ?? '',
                'prettyprint' => $this->prettyPrint,
                'show_alternative' => $this->showAlternative,
            ],
        ]);

        return new OxrExchangesDto($response->getBody()->getContents());
    }

    /**
     * @param string $date Y-m-d format, example 2021-01-01
     * @throws GuzzleException
     * @throws JsonException
     */
    public function getHistorical(string $date, ?string $base, ?string $symbols): OxrExchangesDto
    {
        $response = $this->client->get(sprintf(static::URI_TEMPLATE_HISTORICAL, $date), [
            'query' => [
                'app_id' => $this->appId,
                'base' => $base ?? self::BASE_DEFAULT,
                'symbols' => $symbols ?? '',
                'prettyprint' => $this->prettyPrint,
                'show_alternative' => $this->showAlternative,
            ],
        ]);

        return new OxrExchangesDto($response->getBody()->getContents());
    }

    /**
     * @throws GuzzleException
     * @throws JsonException
     */
    public function getCurrencies(?string $showInactive): OxrCurrenciesDto
    {
        $response = $this->client->get(static::URI_CURRENCIES, [
            'query' => [
                'app_id' => $this->appId,
                'show_inactive' => $showInactive ?? false,
                'prettyprint' => $this->prettyPrint,
                'show_alternative' => $this->showAlternative,
            ],
        ]);

        return new OxrCurrenciesDto($response->getBody()->getContents());
    }

    /**
     * @throws GuzzleException
     * @throws JsonException
     */
    public function getTimeSeries(string  $dateStart,
                                  string  $dateEnd,
                                  ?string $base,
                                  ?string $symbols = ''): OxrTimeSeriesDto
    {
        $response = $this->client->get(static::URI_TIME_SERIES, [
            'query' => [
                'app_id' => $this->appId,
                'start' => $dateStart,
                'end' => $dateEnd,
                'symbols' => $symbols ?? '',
                'base' => $base ?? static::BASE_DEFAULT,
                'prettyprint' => $this->prettyPrint,
                'show_alternative' => $this->showAlternative,
            ],
        ]);

        return new OxrTimeSeriesDto($response->getBody()->getContents());
    }

    /**
     * @throws GuzzleException
     * @throws JsonException
     */
    public function convert(int $value, string $from, string $to): OxrConvertDto
    {
        $response = $this->client->get(sprintf(static::URI_TEMPLATE_CONVERT, $value, $from, $to), [
            'query' => [
                'app_id' => $this->appId,
                'value' => $value,
                'from' => $from,
                'to' => $to,
                'prettyprint' => $this->prettyPrint,
            ],
        ]);

        return new OxrConvertDto($response->getBody()->getContents());
    }

    /**
     * @throws GuzzleException
     * @throws JsonException
     */
    public function getOhcl(string $startTime, string $period, ?string $base, ?string $symbols): OxrOhlcDto
    {
        $response = $this->client->get(static::URI_OHLC, [
            'query' => [
                'app_id' => $this->appId,
                'start_time' => $startTime,
                'period' => $period,
                'symbols' => $symbols ?? '',
                'base' => $base ?? static::BASE_DEFAULT,
                'prettyprint' => $this->prettyPrint,
            ],
        ]);

        return new OxrOhlcDto($response->getBody()->getContents());
    }

    /**
     * @throws GuzzleException
     * @throws JsonException
     */
    public function getUsage(): OxrUsageDto
    {
        $response = $this->client->get(static::URI_USAGE, [
            'query' => [
                'app_id' => $this->appId,
                'prettyprint' => $this->prettyPrint,
            ],
        ]);

        return new OxrUsageDto($response->getBody()->getContents());
    }
}
