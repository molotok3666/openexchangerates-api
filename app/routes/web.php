<?php

use App\Http\Controllers\OpenExchangeRatesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/oxr'], static function () {
    Route::get('/', static function () {
        return redirect()->route('oxr.latest');
    });

    Route::get('latest', [OpenExchangeRatesController::class, 'getLatest'])
        ->name('oxr.latest');
    Route::get('historical', [OpenExchangeRatesController::class, 'getHistorical']);
    Route::get('currencies', [OpenExchangeRatesController::class, 'getCurrencies']);
    Route::get('time-series', [OpenExchangeRatesController::class, 'getTimeSeries']);
    Route::get('convert', [OpenExchangeRatesController::class, 'convert']);
    Route::get('ohlc', [OpenExchangeRatesController::class, 'getOhlc']);
    Route::get('usage', [OpenExchangeRatesController::class, 'getUsage']);
});


