<?php

return [
    'api_url' => 'https://openexchangerates.org/api/',
    'app_id' => env('OXR_APP_ID'),
];
